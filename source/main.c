#include <stdio.h>
#include "dice.h"

int main(){
	int n;
	initializeSeed(); 
	printf("Please, how many faces does your dice have? ");
	scanf("%d", &n);
	printf("Let's roll the dice: %d\n", rollDice(n)); // random num gen
	return 0; // Don't Mess With King Dice
}
